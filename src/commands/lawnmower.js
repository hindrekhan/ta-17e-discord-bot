const gis = require('g-i-s');

let fmsg;

function randomInt(low, high) {
  return Math.floor(Math.random() * (high - low) + low);
}

async function getImgLink(err, res) {
  try {
    if (err) {
      console.log(err);
      return 'Failed to get lawn mower';
    }
    const json = res;

    const ind = json.length;

    const imgNum = randomInt(0, ind);
    const imgLink = json[imgNum].url;

    fmsg.reply(imgLink);

    return true;
  } catch (e) {
    throw (e);
  }
}

module.exports = async (msg, amsg) => {
  if (msg.trim() !== '!showmower') {
    return false;
  }
  fmsg = amsg;

  try {
    gis('niiduk', getImgLink);
    return true;
  } catch (e) {
    console.error('e', e);
    return 'Error getting lawn mowers';
  }
};
