const mower = require('./lawnmower');

describe('mower', () => {
  it('wrong input', async () => {
    const res = await mower('asadfdar sfhdfy asdf');

    expect(res).toBeFalsy();
  });
  it('returns mower', async () => {
    const res = await mower('!showmower');

    expect(res).toBe(true);
  });
});
