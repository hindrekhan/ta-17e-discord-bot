const myname = require('./myname');
const help = require('./help');
const lawnmower = require('./lawnmower');

module.exports = [
  myname,
  help,
  lawnmower,
];
