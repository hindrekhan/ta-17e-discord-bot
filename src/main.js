const Discord = require('discord.js');
const cmd = require('node-cmd');
const client = new Discord.Client();
const express = require('express');
const http = require('http');
const commands = require('./commands');

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async (msg) => {
  console.log(`${msg.author.username}: ${msg.content}`);
  if (msg.content === 'ping') {
    msg.reply('Pong!');
  }
  if (msg.author.bot) {
    // ignore messages from bot's
    return;
  }
  Object.values(commands).forEach(async (command) => {
    const response = await command(msg.content, msg);
    if (response && response !== true) {
      msg.reply(response);
    }
  });
});

client.login(process.env.BOT_TOKEN);

const app = express();
app.get('/', (request, response) => {
  console.log(`${Date.now()} Ping Received`);
  response.sendStatus(200);
});
app.listen(process.env.PORT);
setInterval(() => {
  http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 270000);
setInterval(() => {
  cmd.run('refresh');
}, 3600000);

app.post('/git', (req, res) => {
  if (req.headers['x-gitlab-event'] === 'Push Hook') {
    cmd.run('chmod 777 src/git.sh');
    cmd.get('src/git.sh', (err, data) => {
      if (data) console.log(data);
      if (err) console.log(err);
    });
    cmd.run('refresh');

    console.log('> [GITLAB] Updated with gitlab/master');
  }
  return res.sendStatus(200);
});
